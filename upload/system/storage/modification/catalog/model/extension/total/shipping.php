<?php

class ModelExtensionTotalShipping extends Model
{
    public function getTotal($total)
    {
        if ($this->cart->hasShipping() && isset($this->session->data['shipping_method'])) {
            $total['totals'][] = [
                'code'       => 'shipping',
                'title'      => $this->session->data['shipping_method']['title'],
                'value'      => $this->session->data['shipping_method']['cost'],
                'sort_order' => $this->config->get('shipping_sort_order'),
            ];
            
            //I  could add that a as a separate payment method with dynamic charge,but according to the requirement now,
            // it can be done via pushing to the total array as im doing as follows:
            if ($this->session->data['shipping_method']['code'] == 'tst_xlogistics.tst_xlogistics') {
                $total['totals'][] = [
                    'code'       => 'COD fees',//it should be  in language
                    'title'      => 'Cash on delivery fees',//language
                    'value'      => 15,//it should be dynamic value
                    'sort_order' => '5',//it should be dynamic value
                ];

                $total['total'] += 15;
            }


            if ($this->session->data['shipping_method']['tax_class_id']) {
                $tax_rates = $this->tax->getRates($this->session->data['shipping_method']['cost'], $this->session->data['shipping_method']['tax_class_id']);

                foreach ($tax_rates as $tax_rate) {
                    if (!isset($total['taxes'][$tax_rate['tax_rate_id']])) {
                        $total['taxes'][$tax_rate['tax_rate_id']] = $tax_rate['amount'];
                    } else {
                        $total['taxes'][$tax_rate['tax_rate_id']] += $tax_rate['amount'];
                    }
                }
            }

            $total['total'] += $this->session->data['shipping_method']['cost'];
        }
    }
}